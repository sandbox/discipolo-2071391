#Motivations
1. Sandbox for investigating omega extensions that might later be integrated into omega base
  1. provide basic appearance related functionality without contrib
  2. investigate whether extensions or parts of extensions can depend on contrib and, when enabled pull the relevant contribs settings page into the extensions config form (assuming that a user would expect to go to appearance theme settings to change those theme related contrib module settings) possibly modifying the form or the options provided as well as recomending, explaining and styling adititional contrib (eg: choose to enable style for core toolbar) 
2. an omega 4.x theme for the pushtape distribution as alternative to flux
3. a theme that is usable for non pushtape sites

#Features and potential extensions and relevant contrib

##overlay region
highlighted overlaps hero assuming hero contains slideshow

##colorstyles:
- skinr doesn allow to define new skins with colorpicker but can be integrated but hard to turn into an extension because of adding stuff to .info
- color only global elements and hard to get into an extension
- advanced forum style basic integration is simple (css) templates get kinda confusing
- superfish style - is best simply overidden via css

##sticky footer
there is a compass mixin that could be optionally enabled

##fixed topbar
i want to print a variable or region in my layout that renders a menu containing local tasks, action liks, and a custom menu with home account and node/add into my layout

i have a .tpl.php file that is the menu and am printing the variable in my layout , i have a page preprocess function where i define the variable which works if used in my theme (preprocess folder) but not via the extension. i dont understand why preprocess functions dont work completly in extensions.

- should i do this with a region?
- do i register hook_theme to build the menu via the template? https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_theme/7
should i not use a template but build the menu using a theme_function?

the current implementation just inserts new css for tabs, it should inject a new menu that includes tabs, actionlinks and other menu items into the current page/layout.tpl — i am trying to put a menu into a template by setting a variable to a theme function that uses a template ... is this the right way? is there anything obvious i could be missing?

###options this extension could provide:
- only for admin
- fixed
- offcanvas nav
- additional or replacing


###this extension could interact with contrib:
- toolbar & shortcuts
- admin menu
- nav
- local tasks block

##bgimage
allow changing background image

### could integrate with:

- https://drupal.org/node/177868
- https://drupal.org/project/background (old module, take code)
- dynamic background
- backstretch
- responsive background

#pushtheme specific styles:

- releases
- events
- news
- photos

