<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * Becohming theme.
 */

/**
 * Check if a block is a menu block or not.
 *
 * @param $block
 *  A block object.
 * @return bool
 *  Given block is a menu block.
 */
function _becohming_is_menu_block($block) {
  return in_array($block->module, array('menu', 'menu_block')) || ($block->module == 'system' && !in_array($block->delta, array('help', 'powered-by', 'main')));
}


/**
 * Returns HTML for breadcrumbs
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: 
 *
 * @ingroup themeable
 * TODO: Extend http://drupal.stackexchange.com/questions/72515/how-to-remove-the-home-link-in-menu-breadcrumb-in-drupal
 */
function becohming_breadcrumb($variables) {
  //$theme = omega_get_theme();
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    // Adding the title of the current page to the breadcrumb.
    //$breadcrumb[] = drupal_get_title();
    // only print if more than one

    if (count($breadcrumb) > 2) {
      
      // hide home
      array_shift($breadcrumb);
      // Provide a navigational heading to give context for breadcrumb links to
      // screen-reader users. Make the heading invisible with .element-invisible.
      $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    
      // Override Drupal core breadcrumb item imploding (no arrow).
      $output .= '<ul class="breadcrumb"><li>' . implode('</li><li>', $breadcrumb) . '</li></ul>';
      return $output;
    }
    else {
      unset($breadcrumb);
      $variables['breadcrumb'] = '';
      drupal_set_breadcrumb($variables['breadcrumb']);
      unset($variables['breadcrumb']);
      return '';
    }
  }
  //  if (arg(0)=='taxonomy' && arg(1)=='term') {
  //          $term = taxonomy_term_load(arg(2));
  //          
  //         if ($term->vid==YOUR_VOCABULARY_VID) {
  //              $breadcrumb[] = l($term->vocabulary_machine_name;, 'landing-page-url');
  //          }
  //      }
  

}

function becohming_form_alter( &$form, &$form_state, $form_id ) {


  if (in_array( $form_id, array('user_login', 'user_login_block'))) {
    $form['name']['#attributes']['placeholder'] = t( 'Username or email' );
    $form['pass']['#attributes']['placeholder'] = t( 'Password' );
    $form['name']['#title_display'] = "invisible";
    $form['pass']['#title_display'] = "invisible";
    $form['actions']['submit']['#value'] = t("Hit this to login!");
    //dpm($form);
    $form['#attributes']['class'][] = "tooltip"; // add a class to the form
    // For debug
    //drupal_set_message('<pre>' . check_plain(var_export($form, TRUE)) . '</pre>');

    // Remove the links provided by Drupal.
    unset($form['links']);

    // Set a weight for form actions so other elements can be placed
    // beneath them.
    $form['actions']['#weight'] = 5;

    // Shorter, inline request new password link.
    $form['actions']['request_password'] = array(
      '#markup' => l(t('Lost password'), 'user/password', array('attributes' => array('title' => t('Request new password via e-mail.')))),
    );  

    // New sign up link, with 'cuter' text.
    if (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)) {
      $form['signup'] = array(
        '#markup' => l(t('Register - it’s free!'), 'user/register', array('attributes' => array('class' => 'button', 'id' => 'create-new-account', 'title' => t('Create a new user account.')))),
        '#weight' => 10, 
      );
    }
  }
}

