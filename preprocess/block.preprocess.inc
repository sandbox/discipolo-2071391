<?php

/**
 * Implements hook_preprocess_TEMPLATE().
 */
function becohming_preprocess_block(&$variables) {
  $block = $variables['block'];
  if ($block->region == 'navigation' && _becohming_is_menu_block($block)) {
    $variables['attributes_array']['class'][] = 'block--nav-bar';
  }
}
