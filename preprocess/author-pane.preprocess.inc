<?php



/**
* Implementation of hook_preprocess_author_pane().
*/
function becohming_preprocess_author_pane(&$variables) {
  //only if we enable the extension
  if (omega_extension_enabled('forumstyle') && omega_theme_get_setting('use_forumstyle')) {
    // add roles
    $variables['author_roles'] = _becohming_extension_forumstyle_getroles($variables['account']);
  }
}
