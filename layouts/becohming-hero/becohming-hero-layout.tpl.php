<div<?php print $attributes; ?>>

  <header class="l-header" role="banner">
    <div class="l-constrained">

        <?php if (!empty($becohming_extension_topbar)):?>
          <nav class="topbar"><?php print render($becohming_extension_topbar); ?></nav>
        <?php endif; ?>


      <?php print render($page['header']); ?>
      <?php print render($page['navigation']); ?>
      <div class="l-branding site-branding">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-branding__logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
        <?php endif; ?>
        <?php if ($site_name): ?>
          <a href="<?php print $front_page; ?>" class="site-branding__name" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
        <?php endif; ?>
        <?php if ($site_slogan): ?>
          <h2 class="site-branding__slogan"><?php print $site_slogan; ?></h2>
        <?php endif; ?>
      </div>

      
    </div>
  </header>

  <?php if (!empty($page['hero'])): ?>
    <div class="l-highlighted-wrapper">
      <?php print render($page['hero']); ?>
      <?php print render($page['highlighted']); ?>
    </div>
  <?php endif; ?>

  <div id="l-main" class="l-main l-constrained">
    <a id="main-content"></a>

    <div class="l-content" role="main">
      <?php print render($tabs); ?>
      <?php print $breadcrumb; ?>
      <?php print $messages; ?>
      <?php print render($page['help']); ?>

      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1 class="title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if (isset($authorpic)): ?>
        <?php print $authorpic; ?>
      <?php endif; ?>

      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div>

    <?php print render($page['sidebar_first']); ?>
    <?php print render($page['sidebar_second']); ?>
    <div id='layout-footer'></div>
  </div>

  <footer id="l-footer-wrapper" class="l-footer-wrapper" role="contentinfo">
    <?php print render($page['footer']); ?>
  </footer>

</div>
