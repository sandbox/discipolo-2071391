<?php

/**
 * @file
 * Contains the theme settings form elements for the scripts extension.
 */

/**
 * Implements hook_extension_EXTENSION_theme_settings_form_alter().
 */
function becohming_extension_bgimage_settings_form($element, &$form, $form_state) {

 // Container fieldset
  $element['cover_photo'] = array(
    '#type' => 'fieldset',
    '#title' => t('cover_photo'),
  );
 
  // Default path for image
  $bg_path = omega_theme_get_setting('bg_path');
  if (file_uri_scheme($bg_path) == 'public') {
    $bg_path = file_uri_target($bg_path);
  }
  $element['cover_photo']['default_cover_photo'] = array(
      '#type' => 'checkbox', 
      '#title' => t('Use the default cover photo'), 
      '#default_value' => omega_theme_get_setting('default_cover_photo'), 
      '#tree' => FALSE, 
      '#description' => t('Check here if you want the theme to use the cover photo supplied with it.'),
    );
     $element['cover_photo']['settings'] = array(
      '#type' => 'container', 
      '#states' => array(
        // Hide the cover_photo settings when using the default cover_photo.
        'invisible' => array(
          'input[name="default_cover_photo"]' => array('checked' => TRUE),
        ),
      ),
    );
  // Helpful text showing the file name, disabled to avoid the user thinking it can be used for any purpose.
  $element['cover_photo']['settings']['bg_path'] = array(
    '#type' => 'textfield',
    '#title' => 'Path to background image',
    '#default_value' => omega_theme_get_setting('bg_path'),
    '#disabled' => TRUE,
  );

  // Upload field
  $element['cover_photo']['settings']['bg_upload'] = array(
    '#type' => 'file',
    '#title' => 'Upload background image',
    '#description' => 'Upload a new image for the background.',
  );


    // Attach custom submit handler to the form
  //  $element['#submit'][]= array('becohming_extension_bgimage_theme_settings_form_submit');
    // We need a custom submit handler to store the settings and the current image path.
  $form['#submit'][] = 'becohming_extension_bgimage_settings_form_submit';
$form['#validate'][] = 'becohming_extension_bgimage_settings_form_validate';
    //$element['#validate'][] = array('becohming_extension_bgimage_theme_settings_form_validate');
  return $element;
}

/**
 * Submit handler
 */
function becohming_extension_bgimage_settings_form_submit($form, &$form_state) {
  $settings = array();
  // Get the previous value

  $previous = omega_theme_get_setting('cover_photo_path');
 
  $file = file_save_upload('bg_upload');
  if ($file) {
    $parts = pathinfo($file->filename);
    $destination = 'public://' . $parts['basename'];
    $file->status = FILE_STATUS_PERMANENT;
   
    if(file_copy($file, $destination, FILE_EXISTS_REPLACE)) {
      $_POST['bg_path'] = $form_state['values']['bg_path'] = $destination;
      // If new file has a different name than the old one, delete the old
      if ($destination != $previous) {
        drupal_unlink($previous);
      }
    }
  } else {
    // Avoid error when the form is submitted without specifying a new image
    $_POST['bg_path'] = $form_state['values']['bg_path'] = $previous;
  }
}

/**
 * Validate handler where we actually save the files...
 */
function becohming_extension_bgimage_settings_form_validate($form, &$form_state) {
  // Handle file uploads.
  $validators = array('file_validate_is_image' => array());

  // Check for a new uploaded logo.
  $file = file_save_upload('bg_upload', $validators);
  if (isset($file)) {
    // File upload was attempted.
    if ($file) {
      // Put the temporary file in form_values so we can save it on submit.
      $form_state['values']['bg_upload'] = $file;
//$form_state['values']['cover_photo']['settings']['bg_path'] = $file->url;

//maybe write into filepath here?
    }
    else {
      // File upload failed.
      form_set_error('bg_upload', t('The image could not be uploaded.'));
    }
  }

  $validators = array('file_validate_extensions' => array('ico png gif jpg jpeg apng svg'));

  // If the user provided a path for a logo or favicon file, make sure a file
  // exists at that path.
  if (!empty($form_state['values']['cover_photo']['settings']['bg_path'])) {
  
    $path = becohming_extension_bgimage_theme_settings_validate_path($form_state['values']['cover_photo']['settings']['bg_path']);
    if (!$path) {
      form_set_error('bg_path', t('The custom path is invalid.'));
    }
  }
}


/**
 * Copy of _system_theme_settings_validate_path($path)
 */
function becohming_extension_bgimage_theme_settings_validate_path($path) {

  // Absolute local file paths are invalid.
  if (drupal_realpath($path) == $path) {
    return FALSE;
  }
  // A path relative to the Drupal root or a fully qualified URI is valid.
  if (is_file($path)) {
    return $path;
  }
  // Prepend 'public://' for relative file paths within public filesystem.
  if (file_uri_scheme($path) === FALSE) {
    $path = 'public://' . $path;
  }
  if (is_file($path)) {
    return $path;
  }
  return FALSE;
}
