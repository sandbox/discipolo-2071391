<?php

/**
 * @file
 * Main extension file for the 'topbar' extension.
 */

/**
 * Implements hook_extension_EXTENSION_theme_registry_alter().
 */
function becohming_extension_topbar_theme_registry_alter(&$registry) {
    $registry['html']['includes'][] = drupal_get_path('theme', 'becohming') . '/extensions/topbar/topbar.inc';
    $registry['html']['preprocess functions'][] = 'becohming_extension_topbar_preprocess_page';
    $registry['html']['theme functions'][] = 'becohming_extension_topbar_theme';

}

function becohming_extension_topbar_preprocess_page(&$variables) {

    // local tasks
  $variables['primary_local_tasks'] = $variables['secondary_local_tasks'] = FALSE;
  if (!empty($variables['tabs']['#primary'])) {
    $variables['primary_local_tasks'] = $variables['tabs'];
    unset($variables['primary_local_tasks']['#secondary']);
  }
  if (!empty($variables['tabs']['#secondary'])) {
    $variables['secondary_local_tasks'] = array(
      '#theme' => 'menu_local_tasks',
      '#secondary' => $variables['tabs']['#secondary'],
    );
  }


  //$topbar = array(
   //   '#theme' => 'becohming_extension_topbar',
   //   '#markup' => theme('becohming_extension_topbar_menu'),
   //   '#template' => 'becohming-extension-topbar-menu.tpl.php',
   // );

  //unset ($variables['becohming_extension_topbar']);
  //$variables['becohming_extension_topbar'] = $topbar;
  // try to put the new themed menu into a variable for printing in template
  //doesnt work in extension
  //$variables['becohming_extension_topbar'] = theme('becohming_extension_topbar_menu', $variables);


  // Try to get the theme function/variable to render the template
  $variables['becohming_extension_topbar']['#theme_wrappers']= array('region');
  $variables['becohming_extension_topbar']['#markup'] = 'CUSTOM CONTENT rendered by custom region.tpl.php';
  //$variables['becohming_extension_topbar']['#region'] = 'topbar';
  $variables['becohming_extension_topbar']['#theme_hook_suggestions'][] = 'becohming__extension__topbar__' . 'topbar';
  $variables['becohming_extension_topbar']['#theme_hook_suggestions'][] = 'topbar'; 
  
  $path = drupal_get_path('theme', 'becohming');

 // if (omega_theme_get_setting('toggle_topbar')) {
  drupal_add_css($path . '/css/extensions/topbar/topbar.css', array('group' => CSS_THEME, 'weight' => 12, 'every_page' => TRUE));

  

 // }

}

/**
 * Implements hook_theme().
 * i suspect this doesnt work in an extension either?
 */
function becohming_extension_topbar_theme($existing, $type, $theme, $path) {
  // Create theme entries

  return array(
    // Displays 
    'becohming_extension_topbar_menu' => array(
      'variables' => array(
        'primary_local_tasks' => $primary_local_tasks,
        'secondary_local_tasks' => $secondary_local_tasks,
        'action_links' => $action_links,
        'secondary_menu' => $secondary_menu,

      ),
      'template' => array('becohming__extension__topbar__menu', 'topbar'),
      //'function' => 'theme_becohming_extension_topbar_menu',
      //'render element' => 'becohming_extension_topbar',
      //'file' => 'becohming_extension_topbar.inc',
      'path' => drupal_get_path('theme', 'becohming') . '/extensions/topbar',
    ),
  );
}


// TODO Notice: Undefined index: localized_options in menu_navigation_links() https://drupal.org/node/1020364 https://drupal.org/node/1018614
function theme_becohming_extension_topbar_menu($variables) {
  // i would rather not use this but the template instead

  //dpm('i am the topbar extension theme function');
  if (isset($variables['primary_local_tasks']) || isset($variables['secondary_local_tasks']) || isset($variables['action_links']) || (!$variables['toolbar'] && !$variables['overlay'] && isset($variables['secondary_menu']))) {
    $topbar = '<nav id="tasks" class="container"><div class="tasks"><div class="inner clearfix">' . render($variables['tabs']) . theme('links', array('links' => $variables['main_menu'], 'attributes' => array('class' => 'nav main-nav'))) . theme('links', array('links' => $variables['secondary_menu'], 'attributes' => array('class' => 'nav secondary-nav'))) . '<div class="divider"></div>' . render($variables['action_links']) . '</div></div>' . render($variables['secondary_local_tasks']) . '</nav>';
  }
  return $topbar;
}

