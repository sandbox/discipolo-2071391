<?php print render($tabs) ?>
<?php if ($primary_local_tasks || $secondary_local_tasks || $action_links || (!$toolbar && !$overlay && isset($secondary_menu))): ?>
<nav id="tasks" class="container">
  <div class="tasks">
    <div class="inner clearfix">
      <?php print render($primary_local_tasks) ?>
      <?php if ($primary_local_tasks && $action_links): ?>
        <div class="divider"></div>
      <?php endif; ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links) ?></ul>
      <?php endif; ?>

      <?php if (!$toolbar && !$overlay && isset($secondary_menu)): ?>
        <?php print theme('links', array('links' => $secondary_menu, 'attributes' => array('class' => 'nav secondary-nav'))) ?>
      <?php endif; ?>
    </div>
  </div>
  <?php if ($secondary_local_tasks): ?>
    <div class="sub clearfix"><?php print render($secondary_local_tasks) ?></div>
  <?php endif; ?>
</nav>
<?php endif; ?>
