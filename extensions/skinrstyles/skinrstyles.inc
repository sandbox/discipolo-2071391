<?php

/**
 * @file
 * Main extension file for the 'skinrstyles' extension.
 */

/**
 * Implements hook_extension_EXTENSION_registry_alter().
 */
function becohming_extension_skinrstyles_theme_registry_alter(&$registry) {


//  if (!empty($registry['html']['images'])) {
    $registry['html']['includes'][] = drupal_get_path('theme', 'becohming') . '/extensions/skinrstyles/skinrstyles.inc';
   // $registry['html']['preprocess functions'][] = 'becohming_extension_skinrstyles_preprocess_html';
//     $registry['html']['preprocess functions'][] = 'becohming_extension_skinrstyles_preprocess_page';

//  }
}

function becohming_extension_skinrstyles_system_info_alter(&$info, $file, $type) {
dpm($type);
if ($type == 'theme') {
  // Only fill this in if the .info file does not define a 'datestamp'.
  if (empty($info['skinr']['api'])) {
    $info['skinr']['api'] = 2;
  }
    if (empty($info['skinr']['directory'])) {
    $info['skinr']['directory'] = 'css/extensions/skinrstyles';
    }
  }
}

/*

function becohming_extension_skinrstyles_preprocess_html(&$vars) {
  
    // Add body classes for custom design options
  $settings = array('becohming_body_bg', 'becohming_accent_color', 'becohming_page_bg', 'becohming_secondary_bg', 
    'becohming_tertiary_bg', 'becohming_footer_bg', 'becohming_text_color', 'becohming_link_color', 
    'becohming_page_title_color', 'becohming_block_bg', 'becohming_block_header', 'becohming_block_header_txt',
    'becohming_round_corners', 'becohming_shadows');
  foreach($settings as $setting){
    $class = omega_theme_get_setting($setting);
    if($class != '1'  && $class != '') {
      $vars['classes_array'][] = $class;
    }
    dpm($class);
  }

  if(theme_get_setting('becohming_body_texture') == '1'){ $vars['classes_array'][] = 'txt-bod'; }

  // only load skins.css if skinrstyles module in use
  if(module_exists('skinr')){
     $path = drupal_get_path('theme', $GLOBALS['theme']);
     $filepath = $path . '/css/extensions/skinrstyles/skins.css';
     print $filepath;
       if (file_exists($filepath)) {
         drupal_add_css($filepath, array(
          'preprocess' => TRUE,
          'group' => CSS_THEME,
          'weight' => 1003,
          'media' => 'screen',
          'every_page' => TRUE,
        )
      );
    }
  }
}
*/

