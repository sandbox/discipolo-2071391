<?php

/**
 * @file
 * Contains the theme settings form elements for the scripts extension.
 */

/**
 * Implements hook_extension_EXTENSION_settings_form_alter().
 */
function becohming_extension_skinrstyles_settings_form($element, &$form, $form_state) {


  
  // skinr settings
    $element['skinr'] = array(
      '#type' => 'container', 
      '#title' => t('skin image settings'), 
      '#description' => t('If toggled on, the following skin will be displayed.'),
      '#attributes' => array('class' => array('theme-settings-bottom')),
    );

    $element['skinr']['default_skinr'] = array(
      '#type' => 'checkbox', 
      '#title' => t('Use the default skin'), 
      '#default_value' => omega_theme_get_setting('default_skinr', FALSE, 'becohming'), 
      '#tree' => FALSE, 
      '#description' => t('Check here if you want the theme to use the skin supplied with it.'),
    );
    $element['skinr']['settings'] = array(
      '#type' => 'container', 
      '#states' => array(
        // Hide the skinr settings when using the default skinr.
        'invisible' => array(
          'input[name="default_skinr"]' => array('checked' => TRUE),
        ),
      ),
    );
  //module_load_include('inc', 'skinr', 'skinr_ui.admin');
  //$element['skinr']['admin'] = drupal_render(drupal_get_form('skinr_ui_admin'));
  //$element['skinr']['admin'] = drupal_get_form('skinr_ui_list');
  return $element;
}
