<?php

/**
 * @file
 * Main extension file for the 'forumstyle' extension.
 */

/**
 * Implements hook_extension_EXTENSION_registry_alter().
 */
function becohming_extension_forumstyle_theme_registry_alter(&$registry) {

    $registry['html']['includes'][] = drupal_get_path('theme', 'becohming') . '/extensions/forumstyle/forumstyle.inc';
    //$registry['html']['preprocess functions'][] = 'becohming_extension_forumstyle_preprocess_page';
   // $registry['html']['preprocess_functions'][] = 'becohming_extension_forumstyle_preprocess_forum_list';
    //$registry['advanced_forum_topic_list']['path'] = drupal_get_path('theme', 'becohming') . "/templates/advanced_forum";
    if (omega_extension_enabled('forumstyle') && omega_theme_get_setting('use_forumstyle')) {
      $registry['advanced_forum_author_pane']['path'] = drupal_get_path('theme', 'becohming') . "/templates/advanced_forum";
    }
    //dpm($registry);
}

function becohming_extension_forumstyle_preprocess_page(&$vars) {
  //return becohming_extension_forumstyle_system_info_alter();

//$path = drupal_get_path('theme', 'becohming');

 // if (omega_theme_get_setting('toggle_forumstyle')) {
   // drupal_add_css($path . '/css/extensions/forumstyle/advanced-forum.skydiver.images.css', array('group' => CSS_THEME, 'weight' => 12, 'every_page' => TRUE));
   // drupal_add_css($path . '/css/extensions/forumstyle/advanced-forum.skydiver.style.css', array('group' => CSS_THEME, 'weight' => 12, 'every_page' => TRUE));
 // }

}

function becohming_extension_forumstyle_system_info_alter(&$info, $file, $type) {
if (omega_extension_enabled('forumstyle') && omega_theme_get_setting('use_forumstyle')) {
    // Only fill this in if the .info file does not define a ''plugins' directory.
    if ($type == 'theme') {
      if (empty($info['plugins']['advanced_forum']['styles'])) {
        $info['plugins']['advanced_forum']['styles'] = 'css/extensions/forumstyle/skydiver2';
      }
    }
  }
}
// one option to remove duplicate titles?
function becohming_extension_forumstyle_preprocess_forum_list(&$variables) {
  global $user;
  $row = 0;
  // Sanitize each forum so that the template can safely print the data.
  foreach ($variables['forums'] as $id => $forum) {
    //$variables['forums'][$id]->name = "";
    //unset ($variables['forums'][$id]->name);
  }

}

/**
* Implementation of hook_preprocess_author_pane().
* doesnt work in extension
*/
//function becohming_extension_forumstyle_preprocess_author_pane(&$variables) {
  // Add your variable(s) like this:
 // $variables['becohming_extension_forumstyle_roles'] = becohming_extension_forumstyle_getroles($variables['account']);
//}

/**
* doesnt work in extension so moving to a seperate preprocess file
*/
//function becohming_extension_forumstyle_getroles($variables){
//  $roles = $variables->roles;
 //   foreach ($roles as $key => $value) {
  //    if ($value == 'authenticated user') {
 //       unset($roles[$key]);
 //     }
 //   }
 //   print implode(', ', $roles);
//}

function _becohming_extension_forumstyle_getroles($variables){
  $roles = $variables->roles;
    foreach ($roles as $key => $value) {
      if ($value == 'authenticated user') {
        unset($roles[$key]);
      }
    }
    return implode(', ', $roles);
}
