<?php

/**
 * @file
 * Contains the theme settings form elements for the scripts extension.
 */

/**
 * Implements hook_extension_EXTENSION_settings_form_alter().
 */
function becohming_extension_forumstyle_settings_form($element, &$form, $form_state) {

// Container 
  $element['forumstyle'] = array(
    '#type' => 'container',
    '#title' => t('forumstyle'),
  );

    $element['forumstyle']['use_forumstyle'] = array(
      '#type' => 'checkbox', 
      '#title' => t('Use the forum_style'), 
      '#default_value' => omega_theme_get_setting('use_forumstyle', FALSE, 'becohming'), 
      '#tree' => FALSE, 
      '#description' => t('Check here if you want the theme to use the style supplied with it.'),
    );
    
  return $element;
}


/**
 * Submit handler
 */
function becohming_extension_forumstyle_settings_form_submit($form, &$form_state) {

  $values = $form_state['values'];
  _becohming_extension_forumstyle_system_info_alter();
  

  cache_clear_all(); 
}

function _becohming_extension_forumstyle_system_info_alter(&$info, $file, $type) {
dpm($type);
  // Only fill this in if the .info file does not define a ''plugins' directory.
  if ($type == 'theme') {
    if (empty($info['plugins']['advanced_forum']['styles'])) {
      $info['plugins']['advanced_forum']['styles'] = 'css/extensions/forumstyle/skydiver2';
    }
  }
}
